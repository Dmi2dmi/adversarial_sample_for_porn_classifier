import cv2

img1 = cv2.imread("old_img.png")
img2 = cv2.imread("new_img.png")
noise = img2 - img1

cv2.imwrite("noise.png", noise)
